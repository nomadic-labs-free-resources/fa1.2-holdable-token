# FA1.2 Holdable token

An extension to the FA1.2 standard token that allows tokens to be put on hold. It guarantees a future transfer and makes the held tokens unavailable for transfer in the mean time. Holds are similar to escrows in that are firm and lead to final settlement.

## Global specifications

A hold specifies a sender i.e. a payer, a receiver i.e. a payee, an amount, a notary and an expiration time. When the hold is created, the specified token balance from the payer is put on hold. A held balance cannot be transferred until the hold is either executed or released. The hold can only be executed by the notary, which triggers the transfer of the tokens from the payer to the payee. If a hold is released, either by the notary at any time, or by the sender after the expiration, no transfer is carried out and the amount is available again for the payer.

When defining a hold, sender can define or not the receiver. If defined, upon notary’s execution funds will be transferred to the receiver. If not defined, it will be up to the notary to provide a receiver for execution.

An additional parameter for a hold is also a lock key hereafter named lock hash. The lock hash if not empty, will be stored within the hold entry. Therefore upon execution, notary will have to provide a lock pre-image so that the calulated hash confronted to the lock hash will unlock funds and transfer to the receiver.

## Sequence diagram

In the first scenario, the sender defines a holdable transfer and notary executes the hold.

![Hold setup & Notary executes hold](img/diagram01.png)

Second scenario, sender sets holdable transfer and notary release it.

![Hold setup & Notary releases hold](img/diagram02.png)

Finally, sender sets the holdable transfer and releases it after expiration date is overdue.

![Hold setup & Sender releases hold](img/diagram03.png)

## New entrypoints

To enable the holdable funds and transfers following entrypoints and specifications are introduced.

### hold

The hold entrypoint allows the holder of tokens FA1.2 to set a holdable transfer. Mandatory parameters are amount, notary address and expiration date. Optional parameters are receiver address and lockHash bytes to secure the future execution of the hold.

### executeHold

The executeHold entrypoint allows the notary of a given hold reference i.e. holdId, to execute the transfer on hold. Mandatory parameter is holdId. Optional parameters are receiver address if not specified during the hold and lockPreimage bytes to unlock the hold.

### releaseHold

The releaseHold entrypoint allows the notary of a given hold reference i.e. holdId, to release hold funds back to the sender even before execution date limit. Sender can perform same operation, only if the execution date is overdue. Mandatory parameter is holdId.

### getHoldBalance

The getHoldBalance entrypoint returns current funds in hold for a given address.

### getGrossBalance

The getGrossBalance entrypoint returns current funds in hold and spendable for a given address i.e. total amount of tokens of the given account

### getTotalSupplyOnHold

The getTotalSupplyOnHold entrypoint returns the total amount of tokens currently in hold

### getHoldStatus

The getHoldStatus entrypoint returns the current status (i.e. held, released, executed or non executed) for a given hold entry

________________________________________________________________________________________________________________________________

#### Author
[Sceme](https://sceme.io) - Digital currency management platform

